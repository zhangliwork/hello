package com.zf.zl.hello.mapper;

import com.zf.zl.hello.entity.Resume;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author -----
 * @since 2017-03-23
 */
public interface ResumeMapper extends BaseMapper<Resume> {

}