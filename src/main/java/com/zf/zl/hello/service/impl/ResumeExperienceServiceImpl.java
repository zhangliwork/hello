package com.zf.zl.hello.service.impl;

import com.zf.zl.hello.entity.ResumeExperience;
import com.zf.zl.hello.mapper.ResumeExperienceMapper;
import com.zf.zl.hello.service.IResumeExperienceService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author -----
 * @since 2017-03-23
 */
@Service
public class ResumeExperienceServiceImpl extends ServiceImpl<ResumeExperienceMapper, ResumeExperience> implements IResumeExperienceService {
	
}
