package com.zf.zl.hello.service.impl;

import com.zf.zl.hello.entity.ResumeFieldwork;
import com.zf.zl.hello.mapper.ResumeFieldworkMapper;
import com.zf.zl.hello.service.IResumeFieldworkService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author -----
 * @since 2017-03-23
 */
@Service
public class ResumeFieldworkServiceImpl extends ServiceImpl<ResumeFieldworkMapper, ResumeFieldwork> implements IResumeFieldworkService {
	
}
