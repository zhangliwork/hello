package com.zf.zl.hello.service.impl;

import com.zf.zl.hello.entity.UserDetailInfo;
import com.zf.zl.hello.mapper.UserDetailInfoMapper;
import com.zf.zl.hello.service.IUserDetailInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author -----
 * @since 2017-03-23
 */
@Service
public class UserDetailInfoServiceImpl extends ServiceImpl<UserDetailInfoMapper, UserDetailInfo> implements IUserDetailInfoService {
	
}
