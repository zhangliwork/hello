package com.zf.zl.hello.service.impl;

import com.zf.zl.hello.entity.ResumeWork;
import com.zf.zl.hello.mapper.ResumeWorkMapper;
import com.zf.zl.hello.service.IResumeWorkService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author -----
 * @since 2017-03-23
 */
@Service
public class ResumeWorkServiceImpl extends ServiceImpl<ResumeWorkMapper, ResumeWork> implements IResumeWorkService {
	
}
