package com.zf.zl.hello.service;

import com.zf.zl.hello.entity.UserDetailInfo;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author -----
 * @since 2017-03-23
 */
public interface IUserDetailInfoService extends IService<UserDetailInfo> {
	
}
