package com.zf.zl.hello.service.impl;

import com.zf.zl.hello.entity.UserBase;
import com.zf.zl.hello.mapper.UserBaseMapper;
import com.zf.zl.hello.service.IUserBaseService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author -----
 * @since 2017-03-23
 */
@Service
public class UserBaseServiceImpl extends ServiceImpl<UserBaseMapper, UserBase> implements IUserBaseService {
	
}
