package com.zf.zl.hello.service.impl;

import com.zf.zl.hello.entity.Major;
import com.zf.zl.hello.mapper.MajorMapper;
import com.zf.zl.hello.service.IMajorService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author -----
 * @since 2017-03-23
 */
@Service
public class MajorServiceImpl extends ServiceImpl<MajorMapper, Major> implements IMajorService {
	
}
