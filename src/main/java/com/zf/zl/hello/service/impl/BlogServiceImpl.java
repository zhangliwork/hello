package com.zf.zl.hello.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zf.zl.hello.entity.Blog;
import com.zf.zl.hello.mapper.BlogMapper;
import com.zf.zl.hello.service.IBlogService;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author -----
 * @since 2017-03-23
 */
@Service
public class BlogServiceImpl extends ServiceImpl<BlogMapper, Blog> implements IBlogService {
	
}
