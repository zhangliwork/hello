package com.zf.zl.hello.service.impl;

import com.zf.zl.hello.entity.Resume;
import com.zf.zl.hello.mapper.ResumeMapper;
import com.zf.zl.hello.service.IResumeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author -----
 * @since 2017-03-23
 */
@Service
public class ResumeServiceImpl extends ServiceImpl<ResumeMapper, Resume> implements IResumeService {
	
}
