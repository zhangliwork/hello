package com.zf.zl.hello.service;

import com.zf.zl.hello.entity.Blog;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author -----
 * @since 2017-03-23
 */
public interface IBlogService extends IService<Blog> {
	
}
