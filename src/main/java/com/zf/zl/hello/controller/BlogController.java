package com.zf.zl.hello.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.plugins.Page;
import com.zf.zl.hello.bo.BlogBo;
import com.zf.zl.hello.entity.Blog;
import com.zf.zl.hello.service.IBlogService;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author -----
 * @since 2017-03-23
 */
@Controller
@RequestMapping("/blog")
public class BlogController {
	private static final int PAGE_SIZE = 10;
	@Autowired
	private IBlogService blogService;
	
	@RequestMapping("/index")
	@ResponseBody
	public Map<String, Object> index(int nowPage){
		Map<String, Object> map = new HashMap<>();
		Page<Blog> blogPage = new Page<Blog>(nowPage,PAGE_SIZE);
		blogPage = blogService.selectPage(blogPage);
		map.put("list", blogPage.getRecords());
		return map;
	}
}
