package com.zf.zl.hello.bo;


/**
 * <p>
 * 
 * </p>
 *
 * @author -----
 * @since 2017-03-23
 */
public class BlogBo{

	private Integer id;
	private String themeName;
	private String content;
	private Integer userDetailInfoId;
	private String username;
	private String activeFlag;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getThemeName() {
		return themeName;
	}

	public void setThemeName(String themeName) {
		this.themeName = themeName;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getUserDetailInfoId() {
		return userDetailInfoId;
	}

	public void setUserDetailInfoId(Integer userDetailInfoId) {
		this.userDetailInfoId = userDetailInfoId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}

}
