package com.zf.zl.hello.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import java.io.Serializable;


/**
 * <p>
 * 
 * </p>
 *
 * @author -----
 * @since 2017-03-23
 */
public class Blog extends Model<Blog> {

    private static final long serialVersionUID = 1L;

	private Integer id;
	@TableField("theme_name")
	private String themeName;
	private String content;
	@TableField("user_detail_info_id")
	private Integer userDetailInfoId;
	@TableField("active_flag")
	private String activeFlag;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getThemeName() {
		return themeName;
	}

	public void setThemeName(String themeName) {
		this.themeName = themeName;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getUserDetailInfoId() {
		return userDetailInfoId;
	}

	public void setUserDetailInfoId(Integer userDetailInfoId) {
		this.userDetailInfoId = userDetailInfoId;
	}

	public String getActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
