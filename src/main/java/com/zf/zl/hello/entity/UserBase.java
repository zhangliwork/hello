package com.zf.zl.hello.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;


/**
 * <p>
 * 
 * </p>
 *
 * @author -----
 * @since 2017-03-23
 */
@TableName("user_base")
public class UserBase extends Model<UserBase> {

    private static final long serialVersionUID = 1L;

	@TableId("user_base_id")
	private Integer userBaseId;
	@TableField("login_name")
	private String loginName;
	private String password;
    /**
     * 0 正常 1 冻结
     */
	private Integer status;
    /**
     * 0 普通用户  1 企业用户 2管理员用户
     */
	private Integer role;
	@TableField("active_flag")
	private String activeFlag;


	public Integer getUserBaseId() {
		return userBaseId;
	}

	public void setUserBaseId(Integer userBaseId) {
		this.userBaseId = userBaseId;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getRole() {
		return role;
	}

	public void setRole(Integer role) {
		this.role = role;
	}

	public String getActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}

	@Override
	protected Serializable pkVal() {
		return this.userBaseId;
	}

}
