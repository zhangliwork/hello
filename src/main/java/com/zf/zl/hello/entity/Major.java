package com.zf.zl.hello.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import java.io.Serializable;


/**
 * <p>
 * 
 * </p>
 *
 * @author -----
 * @since 2017-03-23
 */
public class Major extends Model<Major> {

    private static final long serialVersionUID = 1L;

	@TableId("major_id")
	private Integer majorId;
	@TableField("major_name")
	private String majorName;


	public Integer getMajorId() {
		return majorId;
	}

	public void setMajorId(Integer majorId) {
		this.majorId = majorId;
	}

	public String getMajorName() {
		return majorName;
	}

	public void setMajorName(String majorName) {
		this.majorName = majorName;
	}

	@Override
	protected Serializable pkVal() {
		return this.majorId;
	}

}
