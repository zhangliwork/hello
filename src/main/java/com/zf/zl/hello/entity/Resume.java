package com.zf.zl.hello.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import java.io.Serializable;


/**
 * <p>
 * 
 * </p>
 *
 * @author -----
 * @since 2017-03-23
 */
public class Resume extends Model<Resume> {

    private static final long serialVersionUID = 1L;

	@TableId("resume_id")
	private Integer resumeId;
    /**
     * 基本信息
     */
	@TableField("user_detail_info_id")
	private Integer userDetailInfoId;
    /**
     * 个人评价
     */
	@TableField("personal_evaluation")
	private String personalEvaluation;
    /**
     * 状态 0 公开，1非公开
     */
	private Integer status;
	@TableField("active_flag")
	private String activeFlag;


	public Integer getResumeId() {
		return resumeId;
	}

	public void setResumeId(Integer resumeId) {
		this.resumeId = resumeId;
	}

	public Integer getUserDetailInfoId() {
		return userDetailInfoId;
	}

	public void setUserDetailInfoId(Integer userDetailInfoId) {
		this.userDetailInfoId = userDetailInfoId;
	}

	public String getPersonalEvaluation() {
		return personalEvaluation;
	}

	public void setPersonalEvaluation(String personalEvaluation) {
		this.personalEvaluation = personalEvaluation;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}

	@Override
	protected Serializable pkVal() {
		return this.resumeId;
	}

}
