package com.zf.zl.hello.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;


/**
 * <p>
 * 
 * </p>
 *
 * @author -----
 * @since 2017-03-23
 */
@TableName("user_detail_info")
public class UserDetailInfo extends Model<UserDetailInfo> {

    private static final long serialVersionUID = 1L;

	@TableId("user_detail_info_id")
	private Integer userDetailInfoId;
    /**
     * 用户基础ID
     */
	@TableField("user_base_id")
	private Integer userBaseId;
	@TableField("real_name")
	private String realName;
	private Integer age;
	private String phone;
	private String email;
	private String school;
    /**
     * 专业类型
     */
	@TableField("major_id")
	private Integer majorId;
	@TableField("active_flag")
	private String activeFlag;


	public Integer getUserDetailInfoId() {
		return userDetailInfoId;
	}

	public void setUserDetailInfoId(Integer userDetailInfoId) {
		this.userDetailInfoId = userDetailInfoId;
	}

	public Integer getUserBaseId() {
		return userBaseId;
	}

	public void setUserBaseId(Integer userBaseId) {
		this.userBaseId = userBaseId;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSchool() {
		return school;
	}

	public void setSchool(String school) {
		this.school = school;
	}

	public Integer getMajorId() {
		return majorId;
	}

	public void setMajorId(Integer majorId) {
		this.majorId = majorId;
	}

	public String getActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}

	@Override
	protected Serializable pkVal() {
		return this.userDetailInfoId;
	}

}
