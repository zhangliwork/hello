package com.zf.zl.hello.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;


/**
 * <p>
 * 
 * </p>
 *
 * @author -----
 * @since 2017-03-23
 */
@TableName("resume_experience")
public class ResumeExperience extends Model<ResumeExperience> {

    private static final long serialVersionUID = 1L;

	@TableId("resume_experience_id")
	private Integer resumeExperienceId;
	@TableField("project_name")
	private String projectName;
    /**
     * 项目描述
     */
	@TableField("project_detail")
	private String projectDetail;
	@TableField("resume_id")
	private Integer resumeId;
	@TableField("start_time")
	private Date startTime;
	@TableField("end_time")
	private Date endTime;
	@TableField("active_flag")
	private String activeFlag;


	public Integer getResumeExperienceId() {
		return resumeExperienceId;
	}

	public void setResumeExperienceId(Integer resumeExperienceId) {
		this.resumeExperienceId = resumeExperienceId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getProjectDetail() {
		return projectDetail;
	}

	public void setProjectDetail(String projectDetail) {
		this.projectDetail = projectDetail;
	}

	public Integer getResumeId() {
		return resumeId;
	}

	public void setResumeId(Integer resumeId) {
		this.resumeId = resumeId;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}

	@Override
	protected Serializable pkVal() {
		return this.resumeExperienceId;
	}

}
