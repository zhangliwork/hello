package com.zf.zl.hello.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;


/**
 * <p>
 * 
 * </p>
 *
 * @author -----
 * @since 2017-03-23
 */
@TableName("resume_fieldwork")
public class ResumeFieldwork extends Model<ResumeFieldwork> {

    private static final long serialVersionUID = 1L;

	@TableId("resume_fieldwork_id")
	private Integer resumeFieldworkId;
	@TableField("company_name")
	private String companyName;
	@TableField("job_name")
	private String jobName;
	@TableField("work_content")
	private String workContent;
	@TableField("resume_id")
	private Integer resumeId;
	@TableField("start_time")
	private Date startTime;
	@TableField("end_time")
	private Date endTime;
	@TableField("activity_flag")
	private String activityFlag;


	public Integer getResumeFieldworkId() {
		return resumeFieldworkId;
	}

	public void setResumeFieldworkId(Integer resumeFieldworkId) {
		this.resumeFieldworkId = resumeFieldworkId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getWorkContent() {
		return workContent;
	}

	public void setWorkContent(String workContent) {
		this.workContent = workContent;
	}

	public Integer getResumeId() {
		return resumeId;
	}

	public void setResumeId(Integer resumeId) {
		this.resumeId = resumeId;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getActivityFlag() {
		return activityFlag;
	}

	public void setActivityFlag(String activityFlag) {
		this.activityFlag = activityFlag;
	}

	@Override
	protected Serializable pkVal() {
		return this.resumeFieldworkId;
	}

}
